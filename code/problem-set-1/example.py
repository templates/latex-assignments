#!/usr/bin/env python3

names = ['Dmytro', 'George', 'Leo', 'Rich']
print(names)

message = "Hello " + names[0] + "!"
print(message)

message2 = "Hello " + names[1] + "!"
print(message2)

message3 = "Hello " + names[2] + "!"
print(message3)

message4 = "Hello " + names[3] + "!"
print(message4)


def invitation(invitees):
	print()
	for name in invitees:
		print("Dear " + name + ". Please come to dinner!")
	print()


print(f'{names[3]} can not come today.')

del names[3]
names.append('Lorenzo')

invitation(names)

names.insert(0, 'Daria')
names.insert(2, 'Elke')
names.append('Craig')

invitation(names)

print("Sorry. Can invite only two of you((")

while len(names) > 2:
	popped_guest = names.pop()
	print(f'Sorry, {popped_guest}, I can not invite you today')

invitation(names)

while len(names) > 0:
	del names[0]
print(names)
