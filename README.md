# Latex Assignments Template

The build script `build.sh` is capable of building all assignments, in parallel, substituting title and content into shared preamble.
To add new assignment, duplicate a folder in `./assignments` directory and change `meta.tex` and `content.tex` accordingly.

```!bash
# to compile all assignments in parallel
./build.sh

# to compile a specific assignment (eq. ./assignments/problem-set-1/)
./build.sh -c problem-set-1

# to compile in "fast" mode (single iteration, no biber, release environment skipped)
# use -p option
./build.sh -p
./build.sh -p -c problem-set-1

# to control the number of iterations, use -i option
# do not use with -p
./build.sh -i 5
./build.sh -i 5 -c problem-set-1

# option -f is for convenient task functionality in VS Code
# task "Compile once debug" will compile the "current" assignment in "fast" mode
# current is defined by the currently open file (should be in the assignment dir)
```

CI will check for CHKTEX warnings and cSpell errors (use `./vscode/cSpell.json` to add words to dictionary).
You can run tests locally with `./test.sh` (requires chktex and docker).
If tests succeed, it will build and publish the PDFs (per assignment plus merged) as artifacts.

## Quick links (change with you project's URL)

The up-to-date version of the paper is built in CI and resides as artifact.

- [Download the file](https://git.dbogatov.org/templates/latex-assignments/-/jobs/artifacts/master/raw/homework-merged.pdf?job=artifacts)
- [View the file](https://git.dbogatov.org/templates/latex-assignments/-/jobs/artifacts/master/file/homework-merged.pdf?job=artifacts)
