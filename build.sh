#!/usr/bin/env bash

set -e
shopt -s globstar

COMPILER=pdflatex
INTERACTION=nonstopmode
OUTDIR=dist
JOBNAME_PREFIX=homework
ITERATIONS=3
PREVIEW=""
OPEN=false

usage() { echo "Usage: $0 [-c <string> -i <number> -p -f <string> ]" 1>&2; exit 1; }

while getopts "i:c:f:p" o; do
	case "${o}" in
		c)
			ASSIGNMENT=${OPTARG}
			;;
		f)
			ASSIGNMENT=$(basename $OPTARG)
			OPEN=true
			;;
		i)
			ITERATIONS=${OPTARG}
			;;
		p)
			ITERATIONS=1
			PREVIEW="\def\preview{true}"
			;;
		*)
			usage
			;;
	esac
done
shift $((OPTIND-1))

echo "Cleaning up workspace"

rm -f ${OUTDIR}/${JOBNAME_PREFIX}-*.pdf
mkdir -p ${OUTDIR}

if [ -n "$CI_BUILD_REF" ];
then
	printf "\providecommand{\\\version}{%s}" $(echo $CI_BUILD_REF | cut -c1-8) > version.tex
fi

if [ -z "${ASSIGNMENT}" ]
then

	shopt -s dotglob
	shopt -s nullglob
	
	cd assignments
	ASSIGNMENTS=(*/)
	cd ..

	PIDS=()

	for ((i=0;i<${#ASSIGNMENTS[@]};i++))
	do
		{
			echo "Compiling ${ASSIGNMENTS[i]%/} assignment into ${JOBNAME_PREFIX}-${ASSIGNMENTS[i]%/}.pdf ..."
			
			for j in `seq 1 $ITERATIONS`;
			do
				if [ "$j" == "2" ]
				then
					biber ${OUTDIR}/${JOBNAME_PREFIX}-${ASSIGNMENTS[i]%/} &>/dev/null
				fi

				$COMPILER \
					--interaction=${INTERACTION} \
					-output-directory=${OUTDIR} \
					-jobname=${JOBNAME_PREFIX}-${ASSIGNMENTS[i]%/} \
					"\def\assignment{"${ASSIGNMENTS[i]%/}"} ${PREVIEW} \input main.tex" \
					&>/dev/null
			done

			echo "Compiled ${ASSIGNMENTS[i]%/} assignment."
		} &
		PIDS+=($!)
	done

	sleep 1

	echo "Running all tasks in background. Waiting."

	for PID in "${PIDS[@]}"
	do
		wait ${PID}
	done

	echo "All tasks completed successfully."

	echo "Merging all assignments into one file ${JOBNAME_PREFIX}-merged.pdf"

	pdfjoin ${OUTDIR}/${JOBNAME_PREFIX}-*.pdf --outfile ${OUTDIR}/${JOBNAME_PREFIX}-merged.pdf &>/dev/null

else

	echo "Compiling assignment ${ASSIGNMENT} into ${JOBNAME_PREFIX}-${ASSIGNMENT}.pdf ..."

	for j in `seq 1 $ITERATIONS`;
	do
		if [ "$j" == "2" ]
		then
			biber ${OUTDIR}/${JOBNAME_PREFIX}-${ASSIGNMENT}
		fi

		$COMPILER \
			--interaction=${INTERACTION} \
			-output-directory=${OUTDIR} \
			-jobname=${JOBNAME_PREFIX}-${ASSIGNMENT} \
			"\def\assignment{"$ASSIGNMENT"} ${PREVIEW} \input main.tex"
	done
fi

echo "Removing build files..."

rm -f ${OUTDIR}/${JOBNAME_PREFIX}-*.{aux,log,out,xwm,toc,lof,lot,bib,bbl,bcf,blg,xml,thm}

echo "Done!"

if [ "$OPEN" == true ]
then
	open ${OUTDIR}/${JOBNAME_PREFIX}-${ASSIGNMENT}.pdf
fi

exit 0
